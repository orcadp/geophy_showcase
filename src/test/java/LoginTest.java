import io.qameta.allure.Description;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pageobjects.LoginPO;

public class LoginTest extends BaseTest {

    final String LOGIN = "qaskillschallenge@geophy.com";
    final String WRONG_EMAIL = "qaskillschallenge@geophy";
    final String ADDRESS = "555 N. College Avenue, Tempe, AZ, 85281";
    final String NUMBER_OF_UNITS = "200";
    final String OCCUPANCY = "80%";
    final String YEAR_OF_CONSTRUCTION = "2000";
    final String NOI = "2000000";

    final String alertMessage = "The password field is required.";

    @Description("Geophy login page smoke test")
    @Test(description = "Login test")
    public void loginTest() {
        PageFactory.initElements(driver, LoginPO.class)
                .enterLogin(WRONG_EMAIL)
                .getAlertForPass(" ", 3)
                .verifyWarningByText(alertMessage)
                .enterPassword(LOGIN)
                .getAlertForPass(" ", 3)
                .verifyWarningByText(alertMessage)
                .enterLogin(LOGIN)
                .enterPassword(LOGIN)
                .clickMaskPassword()
                .verifyIfPassMasked(false)
                .clickMaskPassword()
                .verifyIfPassMasked(true)
                .clickLogin()
                .verifySearchIsOpened();
    }

    @Description("Geophy search smoke test")
    @Test(description = "Search test")
    public void searchTest() {
        PageFactory.initElements(driver, LoginPO.class)
                .enterLogin(LOGIN)
                .enterPassword(LOGIN)
                .clickLogin()
                .verifySearchIsOpened()
                .verifyResentSearchesAmountDisplayed(5)
                .clickAddressInput()
                .clickNoiInput()
                .verifyAddressRequired(true)
                .enterAddress(ADDRESS)
                .clickNoiInput()
                .enterNoi(NOI)
                .enterNumberOfUnits(NUMBER_OF_UNITS)
                .enterYearOfConstructionINP(YEAR_OF_CONSTRUCTION)
                .clickRun()
                .verifyWaiting()
                .verifyeResult()
        ;
    }
}
