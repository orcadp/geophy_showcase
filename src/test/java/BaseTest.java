import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    Logger log = LoggerFactory.getLogger(BaseTest.class);
    Properties props;
    InputStream is;
    WebDriver driver;
    int timeout;

    public BaseTest() {
        props = new Properties();
        try {
            is = BaseTest.class.getClassLoader().getResourceAsStream("test.properties");
            props.load(is);
        } catch (IOException ioe) {
            log.error(ioe.getMessage());
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioe) {
                    log.error(ioe.getMessage());
                }
            }
        }
    }

    @BeforeTest
    void startBrowser() {
        String fileName = "chromedriver";

        if (!"Linux".equals(System.getProperty("os.name"))) {
            fileName += ".exe";
        }
        System.setProperty("webdriver.chrome.driver", this.getClass().getResource(fileName).getPath());
        timeout = Integer.parseInt(props.getProperty("wait"));

        driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
        driver.get(props.getProperty("base.url"));
    }

    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] doScreenshot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    @AfterMethod
    public void makeScreenshotIfFailed(ITestResult result){
        if(!result.isSuccess()){
            doScreenshot(driver);
        }
    }

    @AfterTest
    void stopDriver() {
        driver.quit();
    }
}
