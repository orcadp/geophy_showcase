package pageobjects;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchPO extends BasePO {

    public SearchPO(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#address_input")
    WebElement addressINP;

    @FindBy(className = "pb-1")
    WebElement evraIMG;

    @FindBy(id = "app")
    WebElement searchResultsFRM;

    @FindBy(xpath = "//span[contains(.,'Please input') and contains(.,'valid')]")
    WebElement addressInputAlertLBL;

    @FindBy(xpath = "//h4[contains(.,'Running Valuation')]")
    WebElement runningValidationLBL;

    @FindBy(xpath = "//*[@type=\"submit\"]")
    WebElement runBTN;

    @FindBy(css = "#noi")
    WebElement noiINP;

    @FindBy(name = "number_of_units")
    WebElement numberOfUnitsINP;

    @FindBy(name = "year_built")
    WebElement yearOfConstructionINP;

    @FindBy(xpath = "//*[@class=\"no-underline text-primary\"]")
    List<WebElement> listOfRecentSearch;

    @Step
    public SearchPO verifySearchIsOpened() {
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        Assert.assertTrue(evraIMG.isDisplayed());
        return this;
    }

    @Step
    public SearchPO verifyWaiting() {
        new WebDriverWait(driver, 2).until(ExpectedConditions.visibilityOf(runningValidationLBL));
        return this;
    }

    @Step
    public SearchPO verifyeResult() {
        driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
        Assert.assertTrue(searchResultsFRM.isDisplayed());
        return this;
    }

    @Step
    public SearchPO verifyResentSearchesAmountDisplayed(long n) {
        Assert.assertEquals(n, listOfRecentSearch.size(), String.format("Collection of elements " +
                "'Recent Searches' size was expected to be " +
                "%s but was %s", String.valueOf(n), String.valueOf(listOfRecentSearch.size())));
        return this;
    }

    @Step
    public SearchPO verifyAddressRequired(boolean isRequired){
        scrollIntoElement(addressINP);
        JavascriptExecutor js = (JavascriptExecutor)driver;
        if (isRequired) {
            Assert.assertEquals(js.executeScript("return window.getComputedStyle(arguments[0]).color;", addressINP).toString(), "rgb(232, 16, 117)");
        } else {
            Assert.assertNotEquals(js.executeScript("return window.getComputedStyle(arguments[0]).color;", addressINP).toString(), "rgb(232, 16, 117)");
        }
        return this;
    }


    @Step
    public SearchPO clickAddressInput(){
        scrollIntoElement(addressINP);
        addressINP.click();
        return this;
    }

    @Step
    public SearchPO clickNoiInput(){
        scrollIntoElement(noiINP);
        noiINP.click();
        return this;
    }

    @Step
    public SearchPO enterAddress(String address){
        scrollIntoElement(addressINP);
        addressINP.sendKeys(address);
        try {
            //acceptable in this case
            Thread.sleep(6000);
            addressINP.sendKeys(Keys.ARROW_DOWN);
            addressINP.sendKeys(Keys.RETURN);
        } catch (InterruptedException ie) {
            Assert.assertTrue(false, String.format("Address %s not found", address));
        }
        return this;
    }

    @Step
    public SearchPO enterNoi(String noi){
        scrollIntoElement(noiINP);
        noiINP.sendKeys(noi);
        return this;
    }

    @Step
    public SearchPO enterNumberOfUnits(String nui){
        scrollIntoElement(numberOfUnitsINP);
        numberOfUnitsINP.sendKeys(nui);
        return this;
    }

    @Step
    public SearchPO clickRun() {
        scrollIntoElement(runBTN);
        runBTN.click();
        return this;
    }

    @Step
    public SearchPO enterYearOfConstructionINP(String year) {
        scrollIntoElement(yearOfConstructionINP);
        yearOfConstructionINP.sendKeys(year);
        return this;
    }
}
