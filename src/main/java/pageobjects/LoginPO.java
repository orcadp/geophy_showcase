package pageobjects;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class LoginPO extends BasePO {
    public LoginPO(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "#email")
    WebElement loginINP;

    @FindBy(css = "#password")
    WebElement passINP;

    @FindBy(xpath = "//button[contains(.,'Log in')]")
    WebElement loginBTN;

    @FindBy(id = "password-icon")
    WebElement passICN;

    @FindBy(className = "checkmark")
    WebElement acceptTermsCHKB;

    @FindBy(className = "checkmark")
    WebElement forgotPassCHKB;

    @FindBy(xpath = "//a[contains(.,'?') and contains(., 'Sign up')]")
    WebElement signupREF;

    @FindBy(xpath = "//a[contains(., 'Forgot')]")
    WebElement forgotPassREF;

    @FindBy(xpath = "//nav//a[contains(., 'Sign up')]")
    WebElement signupMenuBTN;

    @FindBy(xpath = "//nav//a[contains(., 'Login')]")
    WebElement loginMenuBTN;

    @FindBy(css = ".alert")
    WebElement alertAR;

    @Step("Enter login {0}")
    public LoginPO enterLogin(String login) {
        scrollIntoElement(loginINP);
        loginINP.clear();
        loginINP.sendKeys(login);
        return this;
    }

    @Step("Enter password {0}")
    public LoginPO enterPassword(String pass) {
        scrollIntoElement(passINP);
        passINP.clear();
        passINP.sendKeys(pass);
        return this;
    }

    @Step("Verify warning containing {0}")
    public LoginPO verifyWarningByText(String warningLine) {
        return verifyWarningByText(warningLine, 3);
    }

    @Step("Verify warning containing {0} after waiting {1} secs")
    public LoginPO verifyWarningByText(String warningLine, int waitForTextAppearance) {
        driver.manage().timeouts().implicitlyWait(waitForTextAppearance, TimeUnit.SECONDS);
        scrollIntoElement(alertAR);
        Assert.assertTrue(alertAR.isDisplayed());
        Assert.assertTrue(alertAR.getText().contains(warningLine));
        return this;
    }

    @Step("Provoke warning for pass {0}")
    public LoginPO getAlertForPass(String pass, int waitForAlertInSecs) {
        enterPassword(pass);
        clickLogin();
        new WebDriverWait(driver, waitForAlertInSecs).until(ExpectedConditions.visibilityOf(alertAR));
        return this;
    }

    @Step("Verify password is masked")
    public LoginPO verifyIfPassMasked(boolean masked) {
        String typeValue = passINP.getAttribute("type");
        if (masked) {
            Assert.assertEquals(typeValue, "password");
        } else {
            Assert.assertEquals(typeValue, "text");
        }
        return this;
    }

    @Step("Click login")
    public SearchPO clickLogin() {
        scrollIntoElement(loginBTN);
        loginBTN.click();
        return PageFactory.initElements(driver, SearchPO.class);

    }

    @Step("Click mask password")
    public LoginPO clickMaskPassword() {
        scrollIntoElement(passICN);
        passICN.click();
        return this;
    }

    @Step("Click accept terms and conditions")
    public LoginPO acceptTermsAndConditions() {
        scrollIntoElement(acceptTermsCHKB);
        acceptTermsCHKB.click();
        return this;
    }
}
